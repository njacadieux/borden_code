# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 11:07:47 2024

@author: Nicolas Cadieux

GPL-3.0-or-later
This program is free software: you can redistribute it and/or modify it under
the terms of theGNU General Public License as published by the Free Software
Foundation, either version 3 of theLicense, or (at your option) any later
version.This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.You should have received a copy of the GNU
General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
"""


from collections import defaultdict
import geopandas as gpd
import shapely
from shapely import Polygon
import numpy as np

# import sys
# import os
# import fiona

# =============================================================================
#                              Globals
# =============================================================================

EPSG_CODE = 4617
GRID_Z = 0.0
OUTPUT_FILE = r'C:\Users\njaca\Documents\Orientation_Spatiale\Code_Borden\Cadieux2024\BordenCode_2024_test.gpkg'
DENSITY = (1/60)*10  # 10 arc minute # Densify grid with this number.

# =============================================================================
# Latitudinal Degrees denoted by the FIRST CAPITAL LETTERS (N)
# =============================================================================
FIRST_CAPITAL_LETTER = defaultdict(list)
FIRST_CAPITAL_LETTER = {'X': (40.0, 42.0),  # New unit South Ontario
                        'A': (42.0, 44.0),  # South
                        'B': (44.0, 46.0),  # South
                        'C': (46.0, 48.0),  # South
                        'D': (48.0, 50.0),  # South
                        'E': (50.0, 52.0),  # South
                        'F': (52.0, 54.0),  # South
                        'G': (54.0, 56.0),  # South
                        'H': (56.0, 58.0),  # South
                        'I': (58.0, 60.0),  # South
                        'J': (60.0, 62.0),  # South
                        'K': (62.0, 64.0),  # North
                        'L': (64.0, 66.0),  # North
                        'M': (66.0, 68.0),  # North
                        'N': (68.0, 70.0),  # North
                        'O': (70.0, 72.0),  # North
                        'P': (72.0, 74.0),  # North
                        'Q': (74.0, 76.0),  # North
                        'R': (76.0, 78.0),  # North
                        'S': (78.0, 80.0),  # North
                        'T': (80.0, 82.0),  # North
                        'U': (82.0, 84.0),  # North
                        'V': (84.0, 86.0),  # New unit North High-Arctic
                        'W': (86.0, 88.0)}  # New unit North High-Arctic


# =============================================================================
# Longitudinal Degrees denoted by the Second Capital Letters
# Created new basic unit W not in 1952 article
# =============================================================================

SECOND_CAPITAL_LETTER_SOUTH = defaultdict(list)
SECOND_CAPITAL_LETTER_SOUTH = {'-C': (-40.0, -44.0), # not used
                               'Y': (-44.0, -48.0),  # new basic unit east
                               'X': (-48.0, -52.0),  # new basic unit east
                               'A': (-52.0, -56.0),
                               'B': (-56.0, -60.0),
                               'C': (-60.0, -64.0),
                               'D': (-64.0, -68.0),
                               'E': (-68.0, -72.0),
                               'F': (-72.0, -76.0),
                               'G': (-76.0, -80.0),
                               'H': (-80.0, -84.0),
                               'I': (-84.0, -88.0),
                               'J': (-88.0, -92.0),
                               'K': (-92.0, -96.0),
                               'L': (-96.0, -100.0),
                               'M': (-100.0, -104.0),
                               'N': (-104.0, -108.0),
                               'O': (-108.0, -112.0),
                               'P': (-112.0, -116.0),
                               'Q': (-116.0, -120.0),
                               'R': (-120.0, -124.0),
                               'S': (-124.0, -128.0),
                               'T': (-128.0, -132.0),
                               'U': (-132.0, -136.0),
                               'V': (-136.0, -140.0),
                               'W': (-140.0, -144.0)}  # new basic unit west

SECOND_CAPITAL_LETTER_NORTH = defaultdict(list)
SECOND_CAPITAL_LETTER_NORTH = {'B': (-56.0, -64.0),
                               'D': (-64.0, -72.0),
                               'F': (-72.0, -80.0),
                               'H': (-80.0, -88.0),
                               'J': (-88.0, -96.0),
                               'L': (-96.0, -104.0),
                               'N': (-104.0, -112.0),
                               'P': (-112.0, -120.0),
                               'R': (-120.0, -128.0),
                               'T': (-128.0, -136.0),
                               'V': (-136.0, -144.0)}

SECOND_CAPITAL_LETTER_ARCTIC = defaultdict(list)
SECOND_CAPITAL_LETTER_ARCTIC = {'A': (-56.0, -72.0),
                                'F': (-72.0, -88.0),
                                'J': (-88.0, -104.0),
                                'N': (-104.0, -120.0),
                                'R': (-120, -136.0),
                                'V': (-136, -152.0),}

# =============================================================================
#                     Basic Unit with special rules
# =============================================================================
SECOND_CAPITAL_LETTER_ARCTIC_EAST_SB = defaultdict(list)
SECOND_CAPITAL_LETTER_ARCTIC_EAST_SB = {'B': (-64.0, -72.0)}

SECOND_CAPITAL_LETTER_ARCTIC_EAST_TA_UA = defaultdict(list)
SECOND_CAPITAL_LETTER_ARCTIC_EAST_TA_UA = {'A': (-60.0, -72)}

SECOND_CAPITAL_LETTER_ARCTIC_EAST_UX = defaultdict(list)
SECOND_CAPITAL_LETTER_ARCTIC_EAST_UX = {'X': (-56.0, -60)}

# First Capital letter Basic Unit AH Ontario south of 42
FIRST_CAPITAL_LETTER_SOUTH_AH = defaultdict(list)
FIRST_CAPITAL_LETTER_SOUTH_AH = {'A': (41.50, 42.0)}



# =============================================================================
#                 first and seconde small cap lettre
# =============================================================================

FIRST_SMALL_LETTER = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                      'j', 'k', 'l']  # normal first small cap (y)
SECOND_SMALL_LETTER = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                       'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                       'u', 'v', 'w', 'x']  # normal second small cap (x)

# exception first small cap for AH south of 42N
AH_SOUTH_FIRST_SMALL_LETTER = ['o', 'n', 'm']

# =============================================================================
#             Site units size in arc minutes
# =============================================================================

ARC_MINUTES_10 = (1/60*10)  # All latitudes, All longitudes South
ARC_MINUTES_20 = (1/60*20)  # longitudes North
ARC_MINUTES_30 = (1/60*30)  # longitudes Arctic-Artique UA and VA only
ARC_MINUTES_40 = (1/60*40)  # longitudes Arctic-Artique


# =============================================================================
#    Data Field
# =============================================================================

DATA_FIELD_BASIC_UNIT = ['b_basic_Unit', 'zone',
                         'source', 'auteur', 'geometry']
DATA_FIELD_SITE_UNIT = ['basic_unit', 'site_unit', 'zone',
                        'source', 'su_notabene', 'auteur', 'geometry']

def create_polygon(xmin, xmax, ymin, ymax):

    coords = ((xmin, ymin),
              (xmax, ymin),
              (xmax, ymax),
              (xmin, ymax),
              (xmin, ymin))
    pg = Polygon(coords)
    pg = gpd.GeoSeries(pg)
    pg = pg[0].segmentize(DENSITY)
    pg = shapely.force_3d(pg, z=GRID_Z)
    return pg

def create_basic_unit(sorted_keys_fcl,
                      first_cap_ltr,
                      sorted_keys_scl,
                      second_cap_ltr,
                      first_small_ltr,
                      second_small_ltr,
                      zone,
                      source,
                      su_notabene,
                      auteur,
                      step_x,
                      step_y):

    basic_unit_frame = gpd.GeoDataFrame(columns=DATA_FIELD_BASIC_UNIT, geometry='geometry',
                                        crs=EPSG_CODE)
    basic_unit_lst = []  # for printing basic unit list
    site_unit_frame_lst = []

    for f_keys in sorted_keys_fcl:
        ymin = (first_cap_ltr[f_keys][0])
        ymax = (first_cap_ltr[f_keys][1])
        for s_keys in sorted_keys_scl:
            xmin = (second_cap_ltr[s_keys][0])
            xmax = (second_cap_ltr[s_keys][1])
            basic_unit = f_keys + s_keys
            basic_unit_lst.append(basic_unit)
            pg = create_polygon(xmin, xmax, ymin, ymax)
            basic_unit_frame.loc[len(basic_unit_frame.index)] = [
                basic_unit, zone, source, auteur, pg]

            site_unit_frame_lst.append(
                create_site_units(first_small_ltr, second_small_ltr, f_keys,
                                  s_keys, xmin, xmax,
                                  step_x, ymin, ymax,
                                  step_y, zone, source, su_notabene, auteur))

    site_unit_frame = gpd.pd.concat(site_unit_frame_lst)
    print('Creating Borden grid file for :', basic_unit_lst,'\n')
    return basic_unit_frame, site_unit_frame

def create_site_units(fsc, ssc, f_keys, s_keys, xmin, xmax, stepx, ymin,
                      ymax, stepy, zone, source, su_notabene, auteur):

    frame = gpd.GeoDataFrame(columns=DATA_FIELD_SITE_UNIT,
                             geometry='geometry', crs=EPSG_CODE)

    xmin_a = (np.arange(xmin, xmax, -stepx)).round(12)
    xmax_a = (np.arange(xmin-stepx, xmax-stepx, -stepx)).round(12)
    ymin_a = (np.arange(ymin, ymax, stepy)).round(12)
    ymax_a = (np.arange(ymin+stepy, ymax+stepy, stepy)).round(12)
    count_site_units = 0
    fsc_cnt = 0
    for fsc_keys in fsc:
        ssc_cnt = 0
        for ssc_keys in ssc:
            basic_unit = f_keys + s_keys
            site_unit = f_keys + fsc_keys + s_keys + ssc_keys
# =============================================================================
#   Create polygon
# =============================================================================
            pg = create_polygon(xmin_a[ssc_cnt], xmax_a[ssc_cnt]
                                , ymin_a[fsc_cnt], ymax_a[fsc_cnt])

            frame.loc[len(frame.index)] = [basic_unit, site_unit,
                                           zone,
                                           source,
                                           su_notabene,
                                           auteur, pg]
            pg = ()  # Flush GeoSeries
            count_site_units += 1
            ssc_cnt += 1
        fsc_cnt += 1
    return frame

def create_basic_units_south():
    """
    Returns
    -------
    frame : gpd Geodataframe
    """
# =============================================================================
# Standard Borden 1952 Basic units South
# =============================================================================
    basic_unit_frame_lst = []
    site_unit_frame_lst = []
    output_file = OUTPUT_FILE

    # Input variables
    # First Capital Letter
    sorted_keys_fcl = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    # Second Capital Letter

    sorted_keys_scl = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                        'T', 'U', 'V', 'W']

    basic_unit_layer = 'Basic_Unit_South'
    site_unit_layer = 'Site_Unit_South'
    zone = 'Sud-South'
    source = 'Borden 1952'
    su_notabene = 'Compatible with Borden 1952'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_10  # W 10 arc minute
    step_y = ARC_MINUTES_10  # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER
    second_cap_ltr = SECOND_CAPITAL_LETTER_SOUTH
    first_small_ltr = FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)
    
# =============================================================================
# New proposed units south of Borden 1952 Basic units South of 42N
# =============================================================================

    # # Input variables
    # # First Capital Letter
    # sorted_keys_fcl = ['X']
    # # Second Capital Letter
    # sorted_keys_scl = ['B', 'C', 'D']
    # basic_unit_layer = 'Basic_Unit_South'
    # site_unit_layer = 'Site_Unit_South'
    # zone = 'Sud-South'
    # source = 'Nicolas Cadieux 2024'
    # su_notabene = 'New site units compatible with Borden 1952.'
    # auteur = 'Nicolas Cadieux 2024'
    # step_x = ARC_MINUTES_10  # W 10 arc minute
    # step_y = ARC_MINUTES_10 # N 10 arc minute
    # first_cap_ltr = FIRST_CAPITAL_LETTER
    # second_cap_ltr = SECOND_CAPITAL_LETTER_SOUTH
    # first_small_ltr = FIRST_SMALL_LETTER
    # second_small_ltr = SECOND_SMALL_LETTER

    # # Build Basic units and site units
    # basic_unit_frame, site_unit_frame = (
    #     create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
    #                       second_cap_ltr, first_small_ltr, second_small_ltr,
    #                       zone, source, su_notabene, auteur,
    #                       step_x,
    #                       step_y))
    # basic_unit_frame_lst.append(basic_unit_frame)
    # site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
# New proposed units East of Borden 1952
# =============================================================================

    # # Input variables
    # # First Capital Letter
    # sorted_keys_fcl = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
    # # Second Capital Letter
    # sorted_keys_scl = ['Y', 'X']
    # basic_unit_layer = 'Basic_Unit_South'
    # site_unit_layer = 'Site_Unit_South'
    # zone = 'Sud-South'
    # source = 'Nicolas Cadieux 2024'
    # su_notabene = 'New site units'
    # auteur = 'Nicolas Cadieux 2024'
    # step_x = ARC_MINUTES_10  # W 10 arc minute
    # step_y = ARC_MINUTES_10 # N 10 arc minute
    # first_cap_ltr = FIRST_CAPITAL_LETTER
    # second_cap_ltr = SECOND_CAPITAL_LETTER_SOUTH
    # first_small_ltr = FIRST_SMALL_LETTER
    # second_small_ltr = SECOND_SMALL_LETTER

    # # Build Basic units and site units
    # basic_unit_frame, site_unit_frame = (
    #     create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
    #                       second_cap_ltr, first_small_ltr, second_small_ltr,
    #                       zone, source, su_notabene, auteur,
    #                       step_x,
    #                       step_y))
    # basic_unit_frame_lst.append(basic_unit_frame)
    # site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
# Harry J. Bosveld, A SUGGESTED ADJUSTMENT IN THE BORDEN SCHEME OF SITE
# DESIGNATION AS APPLIED TO ONTARIO, Arch Notes 1968, 5.
#
# "With the increasing use of the Borden Scheme of site designation in
# Ontario, it becomes necessary to make an adjustment in order to make the
# scheme applicable to that portion of Ontario which lies south of the 42nd
# parallel of Latitude ( a portion of Essex County and adjacent islands),
# the southerly limits of the Borden Scheme.
# Accordingly, the following adjustment in the scheme is suggested:
# 1) That the area of Canada south of the 42nd parallel is arbitrarily included
# within the area designated by the upper case letters "AH".
# 2) The lower case letters designating longitude present no difficulty,
# as they can be calculated in accordance with the scheme.
# 3) Of the lower case letters designating latitude, "m" has been arbitrarily
# assigned to the area between 41°30' and 41°40T north latitude, "n" to
# the area between 41°40' and 41°50' north latitude, and "o" to the area
# between 41°50' and 42° north latitude.
# 4) Within the system, the lower case letters "a" to "1" inclusive, only,
# are used to designate latitude; therefore, no duplication will result
# from the arbitrary use of "m", "n", and "o" in the manner outlined above.
#
# Harry J. Bosveld,
# Hiram Walker Historical Museum,
# Windsor, Ontario,
# April 5, 1963."

# =============================================================================
    # Input variables
    # First Capital Letter
    sorted_keys_fcl = ['A']
    # Second Capital Letter
    sorted_keys_scl = ['H']
    basic_unit_layer = 'Basic_Unit_South'
    site_unit_layer = 'Site_Unit_South'
    zone = 'Sud-South'
    source = 'Harry J. Bosveld, 1963'
    su_notabene = 'New site units not compatible with Borden 1952.'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_10  # W 10 arc minute
    step_y = ARC_MINUTES_10 # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER_SOUTH_AH
    second_cap_ltr = SECOND_CAPITAL_LETTER_SOUTH
    first_small_ltr = AH_SOUTH_FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
    # concat frames
    site_unit_frame_concat = gpd.pd.concat(site_unit_frame_lst)
    basic_unit_frame_concat = gpd.pd.concat(basic_unit_frame_lst)

    # save to file
    site_unit_frame_concat.to_file(output_file, layer=site_unit_layer)
    basic_unit_frame_concat.to_file(output_file, layer=basic_unit_layer)

    return basic_unit_frame_concat, site_unit_frame_concat

def create_basic_units_north():
    """


    Returns
    -------
    None.

    """
# =============================================================================
# Standard Borden 1952 Basic units North
# =============================================================================
    basic_unit_frame_lst = []
    site_unit_frame_lst = []
    output_file = OUTPUT_FILE

    # Input variables
    # First Capital Letter
    sorted_keys_fcl = ['K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R']
    # Second Capital Letter
    sorted_keys_scl = ['B', 'D', 'F', 'H', 'J', 'L', 'N', 'P', 'R', 'T', 'V']

    basic_unit_layer = 'Basic_Unit_North'
    site_unit_layer = 'Site_Unit_North'
    zone = 'Nord-North'
    source = 'Borden 1952'
    su_notabene = 'Compatible with Borden 1952.  Basic units follow the 1952,\
                    text but not the 1952 index.'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_20 # W 20 arc minute
    step_y = ARC_MINUTES_10  # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER
    second_cap_ltr = SECOND_CAPITAL_LETTER_NORTH
    first_small_ltr = FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
#
# =============================================================================
    # Input variables
    # First Capital Letter
    sorted_keys_fcl = ['S', 'T', 'U']
    # Second Capital Letter
    sorted_keys_scl = ['F', 'H', 'J', 'L', 'N', 'P', 'R', 'T', 'V']

    basic_unit_layer = 'Basic_Unit_North'
    site_unit_layer = 'Site_Unit_North'
    zone = 'Nord-North'
    source = 'Borden 1952'
    su_notabene = 'Compatible with Borden 1952.  Basic units follow the 1952,\
                    text but not the 1952 index.'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_20 # W 20 arc minute
    step_y = ARC_MINUTES_10  # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER
    second_cap_ltr = SECOND_CAPITAL_LETTER_NORTH
    first_small_ltr = FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
#               'SB in 1952 index but should be SD'
# =============================================================================

    # First Capital Letter
    sorted_keys_fcl = ['S']
    # Second Capital Letter
    sorted_keys_scl = ['B']

    basic_unit_layer = 'Basic_Unit_North'
    site_unit_layer = 'Site_Unit_North'
    zone = 'Nord-North'
    source = 'Borden 1952'
    su_notabene = 'SB in 1952 index but should be SD'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_20  # W 20 arc minute
    step_y = ARC_MINUTES_10  # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER
    second_cap_ltr = SECOND_CAPITAL_LETTER_ARCTIC_EAST_SB

    first_small_ltr = FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)


# =============================================================================
    # concat frames
    site_unit_frame_concat = gpd.pd.concat(site_unit_frame_lst)
    basic_unit_frame_concat = gpd.pd.concat(basic_unit_frame_lst)

    # save to file
    site_unit_frame_concat.to_file(output_file, layer=site_unit_layer)
    basic_unit_frame_concat.to_file(output_file, layer=basic_unit_layer)

    return basic_unit_frame_concat, site_unit_frame_concat

def create_basic_units_arctic():

    basic_unit_frame_lst = []
    site_unit_frame_lst = []
    output_file = OUTPUT_FILE

# =============================================================================
#  TA and UA
# =============================================================================

    # Input variables
    # First Capital Letter
    sorted_keys_fcl = ['T', 'U']
    # Second Capital Letter
    sorted_keys_scl = ['A']

    basic_unit_layer = 'Basic_Unit_Arctic'
    site_unit_layer = 'Site_Unit_Arctic'
    zone = 'Arctic-Artique'
    source = 'https://www.pwnhc.ca/download/borden-grid-shapefiles-archive'
    su_notabene = 'Compatible with Borden 1952.  Basic units follow the 1952,\
                    text but not the 1952 index.'
    auteur = 'Nicolas Cadieux 2024'
    step_x = ARC_MINUTES_30  # W 30arc minute
    step_y = ARC_MINUTES_10 # N 10 arc minute
    first_cap_ltr = FIRST_CAPITAL_LETTER
    second_cap_ltr = SECOND_CAPITAL_LETTER_ARCTIC_EAST_TA_UA
    first_small_ltr = FIRST_SMALL_LETTER
    second_small_ltr = SECOND_SMALL_LETTER

    # Build Basic units and site units
    basic_unit_frame, site_unit_frame = (
        create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
                          second_cap_ltr, first_small_ltr, second_small_ltr,
                          zone, source, su_notabene, auteur,
                          step_x,
                          step_y))
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)
# =============================================================================
#   NEW PROPOSED UNIT UX ARCTIQUE EAST
# =============================================================================

    # # Input variables
    # # First Capital Letter
    # sorted_keys_fcl = ['U']
    # # Second Capital Letter
    # sorted_keys_scl = ['X']
    
    # basic_unit_layer = 'Basic_Unit_Arctic'
    # site_unit_layer = 'Site_Unit_Arctic'
    # zone = 'Arctic-Artique'
    # source = 'https://www.pwnhc.ca/download/borden-grid-shapefiles-archive'
    # su_notabene = 'Compatible with Borden 1952.  Basic units follow the 1952,\
    #                 text but not the 1952 index.'
    # auteur = 'Nicolas Cadieux 2024'
    # step_x = ARC_MINUTES_30  # W 30 arc minute
    # step_y = ARC_MINUTES_10  # N 10 arc minute
    # first_cap_ltr = FIRST_CAPITAL_LETTER
    # second_cap_ltr = SECOND_CAPITAL_LETTER_ARCTIC_EAST_UX
    # first_small_ltr = FIRST_SMALL_LETTER
    # second_small_ltr = SECOND_SMALL_LETTER[0:-16]
    # # Build Basic units and site units
    # basic_unit_frame, site_unit_frame = (
    #     create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
    #                       second_cap_ltr, first_small_ltr, second_small_ltr,
    #                       zone, source, su_notabene, auteur,
    #                       step_x,
    #                       step_y))
    # basic_unit_frame_lst.append(basic_unit_frame)
    # site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
#  NEW PROPOSED UNITS Arctic 'VA', 'WA'
# =============================================================================

    # # Input variables
    # # First Capital Letter
    # sorted_keys_fcl = ['V', 'W']
    # # Second Capital Letter
    # sorted_keys_scl = ['A', 'F', 'J', 'N']

    # basic_unit_layer = 'Basic_Unit_Arctic'
    # site_unit_layer = 'Site_Unit_Arctic'
    # zone = 'Arctic-Artique'
    # source = 'https://www.pwnhc.ca/download/borden-grid-shapefiles-archive'
    # su_notabene = 'Compatible with Borden 1952.  Basic units follow the 1952,\
    #                 text but not the 1952 index.'
    # auteur = 'Nicolas Cadieux 2024'
    # step_x = ARC_MINUTES_40  # W 40 arc minute
    # step_y = ARC_MINUTES_10  # N 10 arc minute
    # first_cap_ltr = FIRST_CAPITAL_LETTER
    # second_cap_ltr = SECOND_CAPITAL_LETTER_ARCTIC
    # first_small_ltr = FIRST_SMALL_LETTER
    # second_small_ltr = SECOND_SMALL_LETTER

    # # Build Basic units and site units
    # basic_unit_frame, site_unit_frame = (
    #     create_basic_unit(sorted_keys_fcl, first_cap_ltr, sorted_keys_scl,
    #                       second_cap_ltr, first_small_ltr, second_small_ltr,
    #                       zone, source, su_notabene, auteur,
    #                       step_x,
    #                       step_y))
    # basic_unit_frame_lst.append(basic_unit_frame)
    # site_unit_frame_lst.append(site_unit_frame)


# =============================================================================
    # concat frames
    site_unit_frame_concat = gpd.pd.concat(site_unit_frame_lst)
    basic_unit_frame_concat = gpd.pd.concat(basic_unit_frame_lst)

    # save to file
    site_unit_frame_concat.to_file(output_file, layer=site_unit_layer)
    basic_unit_frame_concat.to_file(output_file, layer=basic_unit_layer)

    return basic_unit_frame_concat, site_unit_frame_concat

if __name__ == "__main__":

    basic_unit_frame_lst = []
    site_unit_frame_lst = []
    output_file = OUTPUT_FILE

    # BUILD Frames South
    basic_unit_frame, site_unit_frame = create_basic_units_south()
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

    # Build Frames North
    basic_unit_frame, site_unit_frame = create_basic_units_north()
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

    # Build Frames Arctic
    basic_unit_frame, site_unit_frame = create_basic_units_arctic()
    basic_unit_frame_lst.append(basic_unit_frame)
    site_unit_frame_lst.append(site_unit_frame)

# =============================================================================
    # concat frames
    basic_unit_frame_concat = gpd.pd.concat(basic_unit_frame_lst)
    site_unit_frame_concat = gpd.pd.concat(site_unit_frame_lst)


    # save to file
    site_unit_frame_concat.to_file(output_file,
                                   layer='Borden_code_site_units')
    basic_unit_frame_concat.to_file(output_file,
                                    layer='Borden_code_basic_units')




