# Borden_Code



## Warning

This is a Beta version of a Python 3.xx code used to generate a Borden Grid file used to name archaeological sites in Canada. This version of the Borden Grid has not been approved and may contain base units and site units not approved by the provinces and territories.  The most current official Borden Grid can be dowloaded from https://www.pwnhc.ca/download/borden-grid-shapefiles-archive/.  Please note that this file is not complete and base unit (H), south of 42N (Ontario), has errors.  Please read the Borden_1952.pdf for the original paper explaining the Borden Grid.



***


## Name
The Borden Python Code

## Description
In construction...

## Visuals
In construction...

## Installation
In construction...

## Usage
In construction...

## Support
In construction...

## Roadmap
In construction...

## Contributing
In construction...

## Authors and acknowledgment
Nicolas Cadieux, ARSL McGill <br />
Borden_1952.pdf courtesy of the BC Archives.

## License
GPL-3.0-or-later
This program is free software: you can redistribute it and/or modify it under
the terms of theGNU General Public License as published by the Free Software
Foundation, either version 3 of theLicense, or (at your option) any later
version.This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.You should have received a copy of the GNU
General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
"""

## Project status
Beta
